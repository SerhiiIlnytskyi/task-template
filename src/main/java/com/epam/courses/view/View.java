package com.epam.courses.view;

import com.epam.courses.view.methods.LanguageViewMethods;
import com.epam.courses.view.methods.MainViewMethods;
import com.epam.courses.view.methods.ViewMethods;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class View {

  private static Scanner INPUT = new Scanner(System.in);
  private static Locale locale = new Locale("en");

  private Map<String, String> menu;
  private ViewMethods viewMethods;

  private ResourceBundle bundle;

  private void setMenu() {
    menu = new LinkedHashMap<>();
    bundle.keySet().stream()
        .sorted((str1, str2) -> new MenuKeysComparator().compare(str1, str2))
        .forEach(key -> menu.put(key, bundle.getString(key)));
  }

  public View(String baseName) {
    this(baseName, locale);
  }

  public View(String baseName, Locale baseLocale) {
    locale = baseLocale;
    bundle = ResourceBundle.getBundle(baseName, locale);
    setMenu();
    switch (Menu.valueOf(baseName.toUpperCase())) {
      case LANGUAGEVIEW:
        viewMethods = new LanguageViewMethods();
        break;
      case MAINVIEW:
        viewMethods = new MainViewMethods();
        break;
      default:
        viewMethods = new MainViewMethods();
    }
  }

  private void button0() {
    System.out.println("Good luck");
    INPUT.close();
    System.exit(0);
  }

  private void outputMenu() {
    System.out.println("\nMenu:");
    menu.forEach((key, value) -> System.out.printf("%s - %s \n", key, value));
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.print("Please, select menu point: ");
      keyMenu = INPUT.nextLine().toUpperCase();
      try {
        viewMethods.getMethodsMenu().get(keyMenu).execute();
      } catch (Exception ignored) {
      }
    } while (!keyMenu.equals("Q"));
    this.button0();
  }
}
