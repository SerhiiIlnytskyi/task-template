package com.epam.courses.view.methods;

import java.util.Map;

public interface ViewMethods {

  Map<String, Executable> getMethodsMenu();
}
